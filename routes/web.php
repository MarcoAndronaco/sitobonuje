<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\FirstController;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'welcome'])->name('welcome');
Route::get('/menu', [PublicController::class, 'menupage'])->name('menupage');

Route::get('/contatti', [FormController::class, 'contacts'])->name('contatti');

Route::post('/contatti/submit', [FormController::class, 'submitContact'])->name('submitContact');
Route::post('/prenotazione/submit', [FormController::class, 'submitReservation'])->name('submitReservation');

Route::get('/menu/form', [MenuController::class, 'menuForm'])->name('menuForm');
Route::post('/menu/submit', [MenuController::class, 'menuSubmit'])->name('menuSubmit');
Route::post('/menu/first/submit', [FirstController::class, 'firstSubmit'])->name('firstSubmit');

// Route::get('/menu/{menu}', [MenuController::class, 'menuDetail'])->name('menuDetail');
// Route::get('/menu/update/{menu}', [MenuController::class, 'menuUpdate'])->name('menuUpdate');
Route::put('/menu/edit/{menu}', [MenuController::class, 'menuEdit'])->name('menuEdit');
Route::delete('/menu/delete/{menu}', [MenuController::class, 'menuDestroy'])->name('menuDestroy');

Route::post('/lingua/{lang}', [PublicController::class, 'setLanguage'])->name('set_language_locale');



