<?php

namespace App\Http\Controllers;

use App\Models\Menù;
use App\Models\First;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function welcome () {
        
        return view('welcome');
    }    
    
    public function menupage () {
        $menùs = Menù::all();
        
        return view('menupage', compact('menùs'));
        
    }    
    
    public function setLanguage($lang){
        
        session()->put('locale', $lang);
        return redirect()->back();
    }
       
}
