<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Mail\ReservationMail;
use App\Mail\AdministratorMail;
use App\Mail\UserReservationMail;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function contacts () {
        return view('contatti');
    }

    public function submitContact (Request $request) {
        $name = $request->input('name');
        $email = $request->input('email');
        $object = $request->input('object');
        $message = $request->input('message');

        $userContact = compact('name', 'message');
        $forAdministrator = compact('name', 'email', 'object', 'message');

        Mail::to($email)->send(new ContactMail($userContact));
        Mail::to('sicilianamente1soccoop@gmail.com')->send(new AdministratorMail($forAdministrator));

        return redirect(route('contatti'))->with('message', 'Grazie per averci contattato, il suo messaggio è stato inviato correttamente.');
    }

    public function submitReservation (Request $request) {
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $number = $request->input('number');
        $data = $request->input('data');
        $time = $request->input('time');

        $userReservation = compact('name', 'email', 'phone', 'number', 'data', 'time');
        $forReservation = compact('name', 'email', 'phone', 'number', 'data', 'time');

        Mail::to($email)->send(new UserReservationMail($userReservation));
        Mail::to('sicilianamente1soccoop@gmail.com')->send(new ReservationMail($forReservation));

        return redirect (route('welcome'))->with('message', 'Grazie per aver prenotato, la prenotazione è andata a buon fine.');
    }
}
