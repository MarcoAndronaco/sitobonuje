<?php

namespace App\Http\Controllers;

use App\Models\Menù;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function menuForm() {
        return view('menuForm');
    }

    public function menuSubmit(Request $request){
        // $menu = new Menù();

        // $menu->title = $request->input('title');
        // $menu->description = $request->input('description');
        // $menu->price = $request->input('price');

        // $menu->save();

        $menu = Menù::create([
            'title'=> $request->input('title'),
            'description'=> $request->input('description'),
            'price'=> $request->input('price'),
            'category'=> $request->input('category'),
        ]);

        return redirect(route('menupage'));
    } 

    // public function menuDetail($menu){
    //     dd($menu);
    //     return view('menu.menuDetail', compact('menù'));
    // }

    // public function menuUpdate(Menù $menu){
    //     return redirect(route('welcome'), compact('menu'));
    // }

    public function menuEdit(Request $req, Menù $menu){
        $menu->title = $req->title;
        $menu->description = $req->description;
        $menu->price = $req->price;
        
        $menu->save();
        return redirect(route('menupage'));
    }

    public function menuDestroy(Menù $menu){
        $menu->delete();
        return redirect(route('menupage'));
    }
    

    
}
