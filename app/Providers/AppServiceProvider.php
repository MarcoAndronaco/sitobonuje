<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $categories = [
            ['id'=>1, 'catName'=>'Antipasti'],
            ['id'=>2, 'catName'=>'Primi'],
            ['id'=>3, 'catName'=>'Secondi'],
            ['id'=>4, 'catName'=>'Contorni'],
            ['id'=>5, 'catName'=>'Pizzeria'],
            ['id'=>6, 'catName'=>'Bevande'],
            ['id'=>7, 'catName'=>'Vini'],
            ['id'=>8, 'catName'=>'Dessert'],
        ];
        View::share('categories', $categories);
    }
}
