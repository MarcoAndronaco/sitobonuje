/*NAVBAR*/
const navbar = document.querySelector('.p-navbar')

window.addEventListener('scroll' , () => {
    // console.log(window.scrollY);
    if(window.scrollY > 0) {
        navbar.classList.add('scrolled')
    } else {
        navbar.classList.remove('scrolled')
    }
})

// SCORRIMENTO PAGINE
function scroll(element){   
    let ele = document.getElementById(element);   
    window.scrollTo(ele.offsetLeft,ele.offsetTop); 
}

// BOTTONE A SCORRIMENTO
const button = document.querySelector('.position-button')

window.addEventListener('scroll' , () => {
    // console.log(window.scrollY);
    if(window.scrollY > 100) {
        button.classList.remove('d-none')
    } else {        
        button.classList.add('d-none')
    }
})

// BOTTONE CATEGORIE
// const buttonCat = document.querySelector('.position-button-category')

// window.addEventListener('scroll' , () => {
//     // console.log(window.scrollY);
//     if(window.scrollY > 100) {
//         buttonCat.classList.remove('d-none')
//     } else {        
//         buttonCat.classList.add('d-none')
//     }
// })

// SCOPRI DI PIU'
let more = document.querySelector('#more')
let arrow = document.querySelector('#arrow')
let more1 = document.querySelector('#more1')
let arrow1 = document.querySelector('#arrow1')
let more2 = document.querySelector('#more2')
let arrow2 = document.querySelector('#arrow2')

more.addEventListener('mouseenter' , () => {
    arrow.style.transform = 'translateX(5px)'
    arrow.style.color = '#e2a600'    
})
more.addEventListener('mouseleave' , () => {
    arrow.style.transform = 'translateX(0px)'
    arrow.style.color = 'black'   
})

more1.addEventListener('mouseenter' , () => {
    arrow1.style.transform = 'translateX(5px)'
    arrow1.style.color = '#e2a600'    
})
more1.addEventListener('mouseleave' , () => {
    arrow1.style.transform = 'translateX(0px)'
    arrow1.style.color = 'black'   
})

more2.addEventListener('mouseenter' , () => {
    arrow2.style.transform = 'translateX(5px)'
    arrow2.style.color = '#e2a600'    
})
more2.addEventListener('mouseleave' , () => {
    arrow2.style.transform = 'translateX(0px)'
    arrow2.style.color = 'black'  
})




