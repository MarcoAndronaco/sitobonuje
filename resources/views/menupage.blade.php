<x-layout>
    <nav class="navbar navbar-expand-lg p-navbar fixed-top" id="navbar">
        <div class="container">
            <a class="navbar-brand fs-1 fw-bold text-warning" href="{{route('welcome')}}"><img src="./img/Logo_bonu_je_bianco.png" class="logo" alt=""></a>        
            <button class="navbar-toggler shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fa-solid fa-bars text-white fs-2"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto mb-2 mb-lg-0">
                    @guest 
                    <li class="nav-item font-1">
                        <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('welcome')}}">Home</a>
                     </li>
                    <li class="nav-item font-1">
                        <a click='scroll("menu");' class="nav-link text-uppercase my-2" href="{{route('menupage')}}#menu">Menù</a>
                    </li>              
                    <li class="nav-item font-1">
                        <a click='scroll("prenota");' class="nav-link text-uppercase my-2" href="{{route('welcome')}}#prenota">{{__('ui.bookTable')}}</a>
                    </li>       
                    <li class="nav-item font-1">
                        <a class="nav-link text-uppercase my-2" href="{{route('contatti')}}">{{__('ui.contacts')}}</a>
                    </li> 
                    @else
                    <li class="nav-item font-1">
                        <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('welcome')}}">Home</a>
                     </li>
                    <li class="nav-item font-1">
                        <a click='scroll("menu");' class="nav-link text-uppercase my-2" href="{{route('welcome')}}#menu">Menù</a>
                    </li>              
                    <li class="nav-item font-1">
                        <a click='scroll("prenota");' class="nav-link text-uppercase my-2" href="{{route('welcome')}}#prenota">{{__('ui.bookTable')}}</a>
                    </li>       
                    <li class="nav-item font-1">
                        <a class="nav-link text-uppercase my-2" href="{{route('contatti')}}">{{__('ui.contacts')}}</a>
                    </li> 
                    <li class="nav-item font-1">
                        <!-- Example single danger button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-navbar dropdown-toggle text-white" data-bs-toggle="dropdown" aria-expanded="false">
                            {{Auth::user()->name}}
                            </button>
                            <ul class="dropdown-menu">
                                <!-- Button trigger modal -->
                                <div class="d-flex justify-content-center">
                                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                        Aggiungi Menù
                                    </button>
                                </div>
                                <a class="nav-link text-dark text-uppercase my-2 d-flex justify-content-center" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">
                                    @csrf
                                </form>                            
                            </ul>
                        </div>                        
                    </li> 
                    @endguest
                </ul>            
            </div>
        </div>
    </nav>
    <section class="container-fluid header-contatti" id="menu">
        <div class="mb-5 content-contatti font-2">
            <h1 class="text-white display-4">Menù</h1>
        </div>
    </section>
    <section id="menu" class="container-fluid px-0 py-5 bg-menu font-2">
        <div class="container py-5" data-aos="fade-up" data-aos-duration="1000">
            {{-- <div class="row mb-5">
                <div class="col-12 d-flex justify-content-center">
                    <h2 class="fw-bold">Menù</h2>
                </div>
            </div> --}}
            @foreach ($categories as $category)
                <div class="row mb-3">
                    <div class="col-12 font-3 d-flex justify-content-center p-0">
                        <button id="" class="btn-click d-flex">
                                                
                            <h2 id="{{$category['catName']}}" class="fw-bold display-4">{{$category['catName']}}</h2>
                            
                               
                        </button>
                    </div>  
                    
                                
                </div>
                <div id="printMenu" class="row justify-content-center">                
                    @foreach ($menùs->where('category', $category['catName']) as $menu)         
                        <div class="col-12 col-md-10 mb-3">
                            <div class="d-flex justify-content-between ">
                                <h5 class="pb-1 text-responsive">{{$menu->title}}</h5>
                                           
                                <h5 class="price text-responsive">{{$menu->price}}</h5>
                            </div>                
                            <p class="border-top text-muted pt-2 text-responsive">{{$menu->description}}</p>
                        </div>
                        @guest                           
                        @else
                            <div class="col-12 col-md-2 mb-5">
                                <div class="row justify-content-center">
                                    <div class="col-3">
                                        <a href="{{route('menuEdit', compact('menu'))}}" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal{{$menu->id}}">
                                            <i class="fa-solid fa-pen-to-square"></i>
                                        </a>
                                    </div> 
                                    <div class="col-3">
                                        <form method="POST" action="{{route('menuDestroy', compact('menu'))}}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa-solid fa-trash"></i>
                                            </button>
                                        </form>
                                    </div>                           
                                    
                                </div>
                                
                            </div>
                        @endguest  
                         
                    
                    @endforeach 
                                
                </div>
            @endforeach
            
        </div>
    </section>

    <a class="position-button-menu" href="#menu"><div class="button-circle"><i class="fa-solid arrow fa-arrow-up"></i></div></a>

    <a class="btn btn-warning button-circle-cat position-button-category" data-bs-toggle="modal" data-bs-target="#modalCategory">
        <i class="fa-solid list-cat fa-list"></i>
    </a>

    

</x-layout>

<!-- Modal Categorie -->
<div class="modal fade" id="modalCategory" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-white" id="exampleModalLabel">Seleziona Categoria</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            
                <div class="row justify-content-center">
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            
                            @foreach ($categories as $category)
                                <a href="#{{$category['catName']}}" class="text-decoration-none d-flex justify-content-center"><button class="btn text-white" type="button" data-bs-dismiss="modal"><nobr>{{$category['catName']}}</nobr></button></a>
                            @endforeach                         
                            
                        </div>
                    </div>                  
                    
                                     
                </div>
            
        </div>
        
    </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-white" id="exampleModalLabel">Inserisci</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{route('menuSubmit')}}">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            <select name="category" id="">
                                @foreach ($categories as $category)
                                    <option value="{{$category['catName']}}">{{$category['catName']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> 
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="title" placeholder="Nome e Cognome" required name="title">
                            <label for="title">Titolo</label>
                        </div>
                    </div>                        
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="description" placeholder="Oggetto" required name="description">
                            <label for="description">Descrizione</label>
                        </div>
                    </div>
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="price" placeholder="Oggetto" required name="price">
                            <label for="price">Prezzo</label>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-warning mb-3 px-5">Salva</button>                             

                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="button" class="btn btn-secondary px-5" data-bs-dismiss="modal">Esci</button>                     

                    </div>                 
                </div>
            </form>
        </div>
        
    </div>
    </div>
</div>   



<!-- Modal Edit-->
@foreach ($menùs as $menu)
<div class="modal fade" id="exampleModal{{$menu->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-white" id="exampleModalLabel">Modifica</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body ">
            <form method="POST" action="{{route('menuEdit', compact('menu'))}}">
                @csrf
                @method('PUT')
                <div class="row justify-content-center">
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="title" placeholder="Nome e Cognome" required name="title" value="{{$menu->title}}">
                            <label for="title">Titolo</label>
                        </div>
                    </div>                        
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="description" placeholder="Oggetto" required name="description" value="{{$menu->description}}">
                            <label for="description">Descrizione</label>
                        </div>
                    </div>
                    <div class="col-12 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="price" placeholder="Oggetto" required name="price" value="{{$menu->price}}">
                            <label for="price">Prezzo</label>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-warning mb-3 px-5">Salva</button>                             

                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="button" class="btn btn-secondary px-5" data-bs-dismiss="modal">Esci</button>                     

                    </div>
                </div>
                    
            </form>
        </div>
        
    </div>
    </div>
</div>   
@endforeach


   

{{-- <div class="col-12  d-flex justify-content-center p-0 border-right">
    <button id="btnPrimi" class="btn-click d-flex">
        <i class="fa-solid color-icons fa-bowl-rice fs-4 me-2"></i>
        <h4 class="fw-bold">Primi</h4>
    </button>
</div>
<div class="col-12 col-md-2 d-flex justify-content-center p-0 border-right">
    <button id="btnSecondi" class="btn-click d-flex">
        <i class="fa-solid color-icons fa-fish fs-4 me-2"></i>
        <h4 class="fw-bold">Secondi</h4>
    </button>
</div>
<div class="col-12 col-md-2 d-flex justify-content-center p-0 border-right">
    <button id="btnPizzeria" class="btn-click d-flex">
        <i class="fa-solid color-icons fa-pizza-slice fs-4 me-2"></i>
        <h4 class="fw-bold">Pizzeria</h4>
    </button>
</div>
<div class="col-12 col-md-2 d-flex justify-content-center p-0 border-right">
    <button id="btnBevande" class="btn-click d-flex">
        <i class="fa-solid color-icons fa-wine-glass fs-4 me-2"></i>
        <h4 class="fw-bold">Bevande</h4>
    </button>
</div>
<div class="col-12 col-md-2 d-flex justify-content-center p-0">
    <button id="btnDessert" class="btn-click d-flex">
        <i class="fa-solid color-icons fa-cheese fs-4 me-2"></i>
        <h4 class="fw-bold">Dessert</h4>
    </button>
</div> --}}