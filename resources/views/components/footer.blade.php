<div class="container-fluid p-0">

  <footer class="bg-footer text-center text-white">
  <!-- Grid container -->
  <div class="container p-4 pb-0">
    <!-- Section: Social media -->
    <section class="my-4">
      <div class="row">
        <div class="col-lg-4 col-xs-12 d-flex align-items-center justify-content-center mb-5">
          <a class="btn btn-outline-light btn-floating m-2 fs-5" href="https://www.facebook.com/bonuje" role="button"><i class="fa-brands fa-facebook"></i></a>
          <a class="btn btn-outline-light btn-floating m-2 fs-5" href="https://www.instagram.com/ristorante_bonuje/" role="button"><i class="fab fa-instagram"></i></a>
          <a class="btn btn-outline-light btn-floating m-2 fs-5" href="https://wa.me/+393880556092" role="button"><i class="fa-brands fa-whatsapp"></i></a>
                    
        </div>  
        

        <div class="col-lg-4 col-xs-12 mb-5">
          <a href=""><img src="/img/Bonu_je_logo_footer.png" width="200px" alt=""></a>
        </div>

        <div class="col-lg-4">
          
            <!-- Links -->
            <h6 class="text-uppercase fw-bold">{{__('ui.contacts')}}</h6>
            <hr
                class="mb-4 mt-0 d-inline-block mx-auto"
                style="width: 60px; background-color: white; height: 2px"
                />
            <p><i class="fas fa-home mr-3"></i> Via Provinciale, 140 , Aci Trezza, CT 95021, IT</p>
            <p><i class="fas fa-envelope mr-3"></i> sicilianamente1soccoop@gmail.com</p>
            <p><i class="fas fa-phone mr-3"></i> +39 388 055 6092 / 095 8320292</p>   
            <div class="d-flex justify-content-center">
              <x-_locale lang='it' nation='it'/>
              <x-_locale lang='en' nation='gb'/>
            </div>  
            
        </div>
      </div>     

      
    </section>
    <!-- Section: Social media -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: #003566;">
    © 2022 Bonu jè
    
  </div>
  <!-- Copyright -->
</footer>
  
</div>




            
        