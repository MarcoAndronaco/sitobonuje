<nav class="navbar navbar-expand-lg p-navbar fixed-top" id="navbar">
    <div class="container">
        <a class="navbar-brand fs-1 fw-bold text-warning" href="{{route('welcome')}}"><img src="./img/Logo_bonu_je.png" class="logo" alt=""></a>        
        <button class="navbar-toggler shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fa-solid fa-bars text-white fs-2"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav m-auto mb-2 mb-lg-0">
                <li class="nav-item font-1">
                    <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('welcome')}}">Home</a>
                </li>
                {{-- <li class="nav-item font-1">
                    <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('register')}}">Registrati</a>
                </li>
                <li class="nav-item font-1">
                    <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('login')}}">Accedi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li> --}}
                <li class="nav-item font-1">
                    <a click='scroll("menu");' class="nav-link text-uppercase my-2" href="#menu">Menù</a>
                </li>              
                <li class="nav-item font-1">
                    <a click='scroll("prenota");' class="nav-link text-uppercase my-2" href="#prenota">Prenota tavolo</a>
                </li>       
                <li class="nav-item font-1">
                    <a class="nav-link text-uppercase my-2" href="{{route('contatti')}}">Contatti</a>
                </li> 
            </ul>            
        </div>
    </div>
</nav>