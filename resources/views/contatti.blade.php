<x-layout>
    <nav class="navbar navbar-expand-lg p-navbar fixed-top" id="navbar">
        <div class="container">
            <a class="navbar-brand fs-1 fw-bold text-warning" href="{{route('welcome')}}"><img src="./img/Logo_bonu_je_bianco.png" class="logo" alt=""></a>        
            <button class="navbar-toggler shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fa-solid fa-bars text-white fs-2"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto mb-2 mb-lg-0">
                    <li class="nav-item font-1">
                        <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('welcome')}}">Home</a>
                     </li>
                    <li class="nav-item font-1">
                        <a click='scroll("menu");' class="nav-link text-uppercase my-2" href="{{route('menupage')}}">Menù</a>
                    </li>              
                    <li class="nav-item font-1">
                        <a click='scroll("prenota");' class="nav-link text-uppercase my-2" href="{{route('welcome')}}#prenota">{{__('ui.bookTable')}}</a>
                    </li>       
                    <li class="nav-item font-1">
                        <a class="nav-link text-uppercase my-2" href="{{route('contatti')}}">{{__('ui.contacts')}}</a>
                    </li> 
                </ul>            
            </div>
        </div>
    </nav>
    <section class="container-fluid header-contatti" id="contatti">
        <div class="mb-5 content-contatti font-2">
            <h1 class="text-white display-4">{{__('ui.contacts')}}</h1>
        </div>
    </section>
    <section>
        {{-- <div class="container mb-5">
            <div class="row">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
                <div class="col-12 mt-5">
                    <div class="font-2">
                        <h1>Contattaci</h1>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="container pb-5 my-5">
            <div class="row">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
                <div class="col-12 col-md-6 mt-5">
                    <div class="text-center font-3 mb-5">
                        <h1 class="display-4 fw-bold">Info</h1>
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-12 col-md-6">
                    <h5 class="text-center font-1 mb-5">{{__('ui.request')}}</h5>
                    <form method="POST" action="{{route('submitContact')}}">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-md-6 mb-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="name" placeholder="Nome e Cognome" required name="name">
                                    <label for="name">{{__('ui.name')}}</label>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-floating">
                                    <input type="email" class="form-control" id="email" placeholder="La tua Email" required name="email">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="object" placeholder="Oggetto" required name="object">
                                    <label for="object">{{__('ui.object')}}</label>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <div class="form-floating">
                                    <textarea class="form-control" placeholder="Messaggio" id="message" style="height: 150px" required name="message"></textarea>
                                    <label for="message">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">{{__('ui.message')}}</font>
                                        </font>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 pb-4 mb-4">
                                {{-- <button class="btn btn-warning button-reservation fs-5 w-100 py-3" type="submit">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">{{__('ui.send')}}</font>
                                    </font>
                                </button> --}}
                                <div class="d-flex justify-content-center">
                                    <button class="custom-btn btn-11 d-flex align-items-center justify-content-center text-uppercase rounded">{{__('ui.send')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12 col-md-6">
                    <h5 class="font-1 mb-5 text-center">{{__('ui.visit')}}</h5>
                    <div class="d-flex justify-content-center">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3162.7127797853254!2d15.158527315572755!3d37.561830232154755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1313fbb818aaa893%3A0xf8b8e2e487f2af92!2sVia%20Provinciale%2C%20140%2C%2095021%20Aci%20Castello%20CT!5e0!3m2!1sit!2sit!4v1653748900707!5m2!1sit!2sit" width="400" height="250" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="my-5 py-1">
        <div class="container my-5">
            <div class="row ">       
                <div class="col-12 col-md-4 text-center mb-5 icon">
                    <i class="fa-solid fa-location-dot fs-3 mb-4"></i>              
                    <h3 class="text-uppercase font-2">{{__('ui.location')}}</h3> 
                    <p class="m-0 p-1">Via Provinciale, 140</p>
                    <p class="m-0 p-1">Aci Trezza, CT 95021</p>              
                </div>

                
                <div class="col-12 col-md-4 text-center mb-5 icon">
                    <i class="fa-solid fa-phone fs-3 mb-4"></i>
                    <h3 class="text-uppercase font-2">{{__('ui.callUs')}}</h3>
                    <p class="m-0 p-1">+39 388 055 6092</p>
                    <p class="m-0 p-1">095 8320292</p>
                </div>

                
                <div class="col-12 col-md-4 text-center icon">
                    <i class="fa-solid fa-clock fs-3 mb-4"></i>
                    <h3 class="text-uppercase font-2">{{__('ui.opening')}}</h3>
                    <p class="m-0 p-1 color-icons">Mar - Ven</p>
                    <p class="m-0 p-1">19:00 - 00:00</p>
                    <p class="m-0 p-1 color-icons">Sab - Dom</p>
                    <p class="m-0 p-1">10:00 - 14:30 / 19:00 - 00:00</p>
                </div>
            </div>
        </div>
    </section>

    <a class="position-button d-none" href="#contatti"><div class="button-circle"><i class="fa-solid arrow fa-arrow-up"></i></div></a>

</x-layout>