<x-layout>
    <nav class="navbar navbar-expand-lg p-navbar fixed-top" id="navbar">
        <div class="container">
            <a class="navbar-brand fs-1 fw-bold text-warning" href="{{route('welcome')}}"><img src="./img/Logo_bonu_je_bianco.png" class="logo" alt=""></a>        
            <button class="navbar-toggler shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fa-solid fa-bars text-white fs-2"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto mb-2 mb-lg-0">
                    @guest            
                    <li class="nav-item font-1">
                        <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('welcome')}}">Home</a>
                    </li>                                       
                    <li class="nav-item font-1">
                        <a click='scroll("menu");' class="nav-link text-uppercase my-2" href="{{route('menupage')}}">Menù</a>
                    </li>              
                    <li class="nav-item font-1">
                        <a click='scroll("prenota");' class="nav-link text-uppercase my-2" href="#prenota">{{__('ui.bookTable')}}</a>
                    </li>       
                    <li class="nav-item font-1">
                        <a class="nav-link text-uppercase my-2" href="{{route('contatti')}}">{{__('ui.contacts')}}</a>
                    </li> 
                    @else
                    {{-- <li class="nav-item font-1">
                        <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('register')}}">Registrati</a>
                    </li> --}}
                    {{-- <li class="nav-item font-1">
                        <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('login')}}">Accedi</a>
                    </li>  --}}
                    <li class="nav-item font-1">
                        <a class="nav-link active text-uppercase tex-color my-2" aria-current="page" href="{{route('welcome')}}">Home</a>
                    </li>                                       
                    <li class="nav-item font-1">
                        <a click='scroll("menu");' class="nav-link text-uppercase my-2" href="{{route('menupage')}}">Menù</a>
                    </li>              
                    <li class="nav-item font-1">
                        <a click='scroll("prenota");' class="nav-link text-uppercase my-2" href="#prenota">{{__('ui.bookTable')}}</a>
                    </li>      
                    <li class="nav-item font-1">
                        <a class="nav-link text-uppercase my-2" href="{{route('contatti')}}">{{__('ui.contacts')}}</a>
                    </li> 
                    <li class="nav-item font-1">
                        <!-- Example single danger button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-navbar dropdown-toggle text-white" data-bs-toggle="dropdown" aria-expanded="false">
                            {{Auth::user()->name}}
                            </button>
                            <ul class="dropdown-menu">
                                <!-- Button trigger modal -->
                                <div class="d-flex justify-content-center">
                                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                        Aggiungi Menù
                                    </button>
                                </div>
                                <a class="nav-link text-dark text-uppercase my-2 d-flex justify-content-center" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">
                                    @csrf
                                </form>                            
                            </ul>
                        </div>                        
                    </li>                    
                    
                    @endguest
                </ul>            
            </div>
        </div>
    </nav>
    
    <header id="welcome">
                    
        <div class="header">
            <div class="container mt-5">
                <div class="row">
                    <div class="mt-5 pt-5">
                        @if (session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                        @endif     
        
                    </div>

                </div>
            </div> 
            <div class="content">         
                <img src="/img/Logo_bonu_je_header.png" alt="">
                <h4 class="font-2">{{__('ui.restaurant')}}</h4>
                {{-- <div class="hero">
                    <a href="#" class="scroll-down">
                        <div class="mouse">
                            <span></span>
                        </div>
                        
                    </a>
                </div> --}} 
                
                {{-- <div class="cont-promo">
                    <div id="promo" class="button-promo">
                        <h5 class="text-white m-0">PROMO</h5>
                    </div>
                    <div id="text" class="description-promo">
                        <div class="promo-text">
                            <h4>-15% su menù pizzeria</h4>
                            <h4>-20% su menù ristorante</h4>
                        </div>
                    </div>
                </div>                  --}}
            </div> 
                         
        </div>       
        
    </header>    

    <section class="my-5 py-5" >
        <div class="container mt-5 font-2 overflow-hidden">
            <div class="row g-5 align-items-center">
                <div class="col-lg-6">
                    <div class="row g-3">
                        <div class="col-6 text-start" data-aos="zoom-in"> 
                            <img src="/img/Immagine_bonuje.png" alt="" class="img-fluid rounded w-100">
                        </div>
                        <div class="col-6 text-start" data-aos="zoom-in">
                            <img src="/img/Immagine_bonuje1.png" alt="" class="img-fluid rounded w-75 margin-top">
                        </div>     
                        <div class="col-6 text-end" data-aos="zoom-in">
                            <img src="/img/Immagine_bonuje2.png" alt="" class="img-fluid rounded w-75">
                        </div>     
                        <div class="col-6 text-end" data-aos="zoom-in">
                            <img src="/img/Immagine_bonuje3.png" alt="" class="img-fluid rounded w-100">
                        </div>                 
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d-flex justify-content-center mb-5" data-aos="zoom-in-up">
                        <img src="/img/Bonu_je_arcobaleno.png" width="300px" alt="">
                    </div>
                    
                    <h1 class="text-center my-5 font-3 fw-bold display-4" data-aos="zoom-in-up">{{__('ui.welcome')}}</h1>
                    <div data-aos="zoom-in-up">
                        <p class="text-center m-0 pb-2 color-cit">" È più facile essere fedele a un ristorante che a una donna. "</p>
                        <p class="text-center text-muted mb-5"><strong>- Federico Fellini -</strong></p>
                        <p class="text-center m-0 pb-2 color-cit">" L'uomo non è fatto per prendere decisioni. Basta vederlo al ristorante, davanti a un menù. "</p>
                        <p class="text-center text-muted mb-5"><strong>- Roberto Gervaso, Il grillo parlante -</strong></p>
                        <p class="text-center m-0 pb-2 color-cit">" La vita non è un ristorante ma un buffet. Lasciatevi servire. "</p>
                        <p class="text-center text-muted"><strong>- Dominique Glocheux, La vita è bella -</strong></p>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container py-5 my-5 overflow-hidden">
            <div class="row">
                <div class="col-12" data-aos="zoom-in">
                    <h1 class="text-center font-3 fw-bold display-4">{{__('ui.special')}}</h1>
                </div>
            </div>
            <div class="row mt-5" data-aos="fade-right" style="transition: 1000ms">
                <div class="col-md-6 mb-5">
                    <div class="pt-4 mt-4">
                        <h2 class="text-center font-2 pb-3">Frittura al metro</h2>
                        <p class="text-muted text-center font-2 mb-4">Il suo profumo inconfondibile si riconosce lontano un miglio ed è uno dei piatti più ordinati nei ristoranti di pesce. Con il <strong>Fritto Misto di Pesce</strong> riuscirete a soddisfare il vostro palato in pochi istanti. Le nostre portate di varie dimensioni <strong>(25cm, mezzo metro, 1 metro)</strong> permettono di gustare questa pietanza in compagnia di amici e parenti.</p>
                        <div class="row justify-content-center">
                            <div class="col-5">
                                <a id="more" href="{{route('menupage')}}" class="text-decoration-none d-flex justify-content-center link">{{__('ui.findOutMore')}}<span id="arrow" class="ms-1"><i class="fa-solid fa-arrow-right-long"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex justify-content-center">
                    <div class="card-special rounded">
                        <img src="/img/frittura_al_metro.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row pt-4 mt-5" data-aos="fade-left" style="transition: 1000ms">
                <div class="col-md-6 d-flex justify-content-center order-resp-2">
                    <div class="card-special rounded">
                        <img src="/img/carosello_mare.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 mb-5 order-resp-1">
                    <div class="pt-4 mt-4">
                        <h2 class="text-center font-2 pb-3">Carosello di mare</h2>
                        <p class="text-muted text-center font-2 mb-4">Spesso la qualità di un ristorante di pesce viene giudicata in base alla bontà e freschezza degli antipasti di mare... le tante varietà di pesce, crostacei e molluschi che la compongono e la caratterizzano, l'hanno resa una vera e propria istituzione della cucina italiana. Il nostro <strong>Carosello di Mare</strong> è composto da portate di <strong>antipasti freddi e caldi</strong>. Una pietanza di cui non potete farne a meno!</p>
                        <div class="row justify-content-center">
                            <div class="col-5">
                                <a id="more1" href="{{route('menupage')}}" class="text-decoration-none d-flex justify-content-center link">{{__('ui.findOutMore')}}<span id="arrow1" class="ms-1"><i class="fa-solid fa-arrow-right-long"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-4 mt-5" data-aos="fade-right" style="transition: 1000ms">
                <div class="col-md-6 mb-5">
                    <div class="pt-4 mt-4">
                        <h2 class="text-center font-2 pb-3">Pizza ai Frutti di Mare</h2>
                        <p class="text-muted text-center font-2 mb-4">La <strong>Pizza ai Frutti di Mare</strong> è una specialità tutta italiana. La pizza ai frutti di mare ha una base di pizza al pomodoro dove vengono riversati frutti di mare come <strong>gamberi, vongole, cozze e tanto altro</strong>. E' una pizza davvero particolare che accontenterà i palati più raffinati.</p>
                        <div class="row justify-content-center">
                            <div class="col-5">
                                <a id="more2" href="{{route('menupage')}}" class="text-decoration-none d-flex justify-content-center link">{{__('ui.findOutMore')}}<span id="arrow2" class="ms-1"><i class="fa-solid fa-arrow-right-long"></i></span></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-6 d-flex justify-content-center">
                    <div class="card-special rounded">
                        <img src="/img/pizza_frutti_di_mare.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    {{-- <section id="prenota" class="my-5 pt-5 pb-5">
        <div class="container mt-5">
            <div class="row">
                <a href="" class="col-12 col-md-3 d-flex justify-content-center mb-4">
                    <div class="card-services text-center font-1">
                        <i class="fa-solid fa-headset fs-1 pt-4"></i>
                        <h5 class="fs-3 py-2 fw-bold">Servizio 24/7</h5>
                        <p class="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Et cum quos repellat optio nam magni ducimus nobis.</p>
                    </div>
                </a>
                <a href="" class="col-12 col-md-3 d-flex justify-content-center mb-4">
                    <div class="card-services text-center font-1">
                        <i class="fa-solid fa-headset fs-1 pt-4"></i>
                        <h5 class="fs-3 py-2 fw-bold">Servizio 24/7</h5>
                        <p class="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Et cum quos repellat optio nam magni ducimus nobis.</p>
                    </div>
                </a>
                <a href="" class="col-12 col-md-3 d-flex justify-content-center mb-4">
                    <div class="card-services text-center font-1">
                        <i class="fa-solid fa-list-check fs-1 pt-4"></i>
                        <h5 class="fs-3 py-2 fw-bold">Prenota tavolo</h5>
                        <p class="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Et cum quos repellat optio nam magni ducimus nobis.</p>
                    </div>
                </a>
                <a href="" class="col-12 col-md-3 d-flex justify-content-center mb-4">
                    <div class="card-services text-center font-1">
                        <i class="fa-solid fa-envelope-circle-check fs-1 pt-4"></i>
                        <h5 class="fs-3 py-2 fw-bold">Servizio 24/7</h5>
                        <p class="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Et cum quos repellat optio nam magni ducimus nobis.</p>
                    </div>
                </a>
            </div>
        </div>
        
    </section> --}}

    <section id="prenota" class="container-fluid p-0 bg-reservation">
        <div class="container" data-aos="zoom-in-up">
            <div class="row">                
                <div class="col-12 mt-5 pt-5">
                    <h2 class="text-center text-white font-2">{{__('ui.book')}} <em>{{__('ui.your')}}</em> {{__('ui.table')}}</h2>
                </div>
            </div>
            <div class="row mt-5"> 
                <div class="col-12 d-flex justify-content-center">
                    <form method="POST" action="{{route('submitReservation')}}">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-md-4 mb-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="name" placeholder="Nome e Cognome" required name="name">
                                    <label for="name">{{__('ui.name')}}</label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-floating">
                                    <input type="email" class="form-control" id="email" placeholder="Email" required name="email">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-floating">
                                    <input type="tel" class="form-control" id="phone" placeholder="Telefono" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" required name="phone">
                                    <label for="phone">{{__('ui.phone')}}</label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-floating">
                                    <input type="date" class="form-control" id="data" placeholder="Data" min="2022-01-01" max="2030-12-31" required name="data"> 
                                    <label for="data">{{__('ui.date')}}</label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="form-floating">
                                    <input type="time" class="form-control" id="time" placeholder="Orario" min="11:30" max="23:00" required name="time">
                                    <label for="time">{{__('ui.hours')}}</label>
                                </div>
                            </div>   
                            <div class="col-md-4 mb-3">
                                <div class="form-floating">
                                    <input type="number" class="form-control" id="number" placeholder="N. persone" min="1" max="30" required name="number">
                                    <label for="number">{{__('ui.person')}}</label>
                                </div>
                            </div> 
                            <div class="col-md-4 pb-4 mb-4 mt-3">
                                {{-- <button class="btn btn-warning button-reservation w-100 py-3" type="submit">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">{{__('ui.bookNow')}}</font>
                                    </font>
                                </button>   --}}
                                <div class="d-flex justify-content-center">
                                    <button class="custom-btn btn-11 d-flex align-items-center justify-content-center text-uppercase rounded">{{__('ui.bookNow')}}</button>
                                </div>
                                
                                                          
                            </div>
                        </div>
                        
                            
                    </form>
                </div>              
                
                
            </div>
        </div>
    </section>
    
    <a class="position-button d-none" href="#welcome"><div class="button-circle"><i class="fa-solid arrow fa-arrow-up"></i></div></a>


    {{-- MODAL MENU --}}   
    
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="exampleModalLabel">Inserisci</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('menuSubmit')}}">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="col-12 mb-3">
                            <div class="form-floating">
                                <select name="category" id="">
                                    @foreach ($categories as $category)
                                        <option value="{{$category['catName']}}">{{$category['catName']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        <div class="col-12 mb-3">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="title" placeholder="Nome e Cognome" required name="title">
                                <label for="title">Titolo</label>
                            </div>
                        </div>                        
                        <div class="col-12 mb-3">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="description" placeholder="Oggetto" required name="description">
                                <label for="description">Descrizione</label>
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="price" placeholder="Oggetto" required name="price">
                                <label for="price">Prezzo</label>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-warning mb-3 px-5">Salva</button>                             

                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="button" class="btn btn-secondary px-5" data-bs-dismiss="modal">Esci</button>                     

                        </div>                 
                    </div>
                </form>
            </div>
            
        </div>
        </div>
    </div>   

    <script>
        let promo = document.querySelector('#promo')
        let text = document.querySelector('#text')

        promo.addEventListener('mouseenter', () => {
            text.style.transform = 'translateX(0px)'
            text.style.opacity = '1'
        })

        promo.addEventListener('mouseleave', () => {
            text.style.transform = 'translateX(-100px)'
            text.style.opacity = '0'
        })
    </script>
    
    
    
</x-layout>



